#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = u'Leandro Toledo'
SITENAME = u"Leandro Toledo's Blog"
SITEDESCRIPTION = u''
SITEURL = 'https://leandrotoledo.com.br'
TIMEZONE = 'America/Sao_Paulo'

ARTICLE_URL = "{date:%Y}/{date:%m}/{slug}"
ARTICLE_SAVE_AS = "{date:%Y}/{date:%m}/{slug}/index.html"
TAG_URL = "tag/{name}"
TAG_SAVE_AS = "tag/{name}/index.html"

DEFAULT_LANG = u'en'
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False
STATIC_PATHS = ['images']
FILES_TO_COPY = (('extra/robots.txt', 'robots.txt'),
                 ('extra/favicon.ico', 'favicon.ico'),)

FEED_ATOM = 'feeds/all.rss.xml'
CATEGORY_FEED_ATOM = 'feeds/categories/%s.rss.xml'
TAG_FEED_ATOM = 'feeds/tags/%s.rss.xml'

GOOGLE_ANALYTICS = 'UA-16485022-3'

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

THEME = 'themes/tuxlite_tbs'
