Template Article
################
:author: Leandro Toledo
:date: 2013-03-20 12:00
:category: GNU/Linux
:tags: tag1, tag2
:slug: template-article
:summary: How to create an article

Introduction
============

Project [1]_  ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae
metus vitae tortor commodo tincidunt eu a turpis. Suspendisse at facilisis
augue. Praesent vitae vulputate diam. Quisque orci diam, gravida in cursus at,
molestie et leo. Praesent egestas dui non lacus molestie venenatis. Ut vitae
feugiat tortor. Integer venenatis dapibus faucibus. Ut feugiat dignissim
accumsan. Cras nec fermentum tellus. Ut imperdiet congue elit ut malesuada.
Morbi ac nisi libero. 

Configuration
=============

Morbi nunc odio, scelerisque sit amet dignissim in, convallis non nulla.
Phasellus in nisl non tellus venenatis dapibus.

.. code-block:: bash

   $ su -c 'vi /dir/to/foo'

.. code-block:: text

   foo {
        ...
        foo on;
        ...
   }

Installation
============

Etiam et orci tellus. Cum sociis natoque penatibus et magnis dis parturient
montes, nascetur ridiculus mus.

.. code-block:: bash

    $ su -c 'aptitude install foo'

Usage
=====

Maecenas pulvinar porttitor nunc nec aliquam. Proin volutpat justo malesuada
augue consectetur in tempus turpis scelerisque.

.. code-block:: bash

    $ foo -arg 1 -arg 2

.. [1] `Foo home page <http://www.foo.org/>`_
