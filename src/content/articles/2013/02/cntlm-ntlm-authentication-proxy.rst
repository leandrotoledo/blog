CNTLM: NTLM authentication proxy
################################
:author: Leandro Toledo
:date: 2013-02-21 11:30
:category: GNU/Linux
:tags: sysadmin, howtos
:slug: cntlm-ntlm-authentication-proxy
:summary: Using CNTLM to authenticate NTLM/NTLM Session Response/NTLMv2 authentication HTTP proxy.


Introduction
============

CNTLM [1]_ is an NTLM/NTLM Session Response/NRLMv2 authentication HTTP proxy intended to help you break free from the chains of Microsoft proprietary world.

Installation
============

Download the `lastest version <http://sourceforge.net/projects/cntlm/files/cntlm>`_ of CNTLM and install it.

.. code-block:: bash

    $ wget "http://sourceforge.net/projects/cntlm/files/cntlm/cntlm%200.92.3/cntlm_0.92.3_i386.deb/download" -O cntlm.deb
    $ su -c 'dpkg -i cntlm.deb'

Configuration
=============

Now, you need to edit the configuration file.

.. code-block:: bash

    $ su -c 'vi /etc/cntlm.conf'
    Username 	<username>
    Domain 	<domain>
    Password 	<password>
    Proxy 	<proxy_ip>:<proxy_port>
    Listen 	3128

You should use an hash password for security reasons.

.. code-block:: bash

    $ su -c 'cntlm -c /etc/cntlm.conf -H'
    Password:
    PassLM      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    PassNT      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    PassNTLMv2  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX    # Only for user '<username>', domain '<domain>'
    $ su -c 'vi /etc/cntlm.conf'
    Username    <username>
    Domain      <domain>
    PassLM      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    PassNT      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    PassNTLMv2  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    Proxy       <proxy_ip>:<proxy_port>
    Listen      3128

Don't forget to restart the service.

.. code-block:: bash

    $ su -c 'service cntlm restart'


Usage
=====

Finally, you'll be able to connect trough CNTLM proxy, but some proxy settings are required. 

Examples:

Firefox/Iceweasel
~~~~~~~~~~~~~~~~~

.. figure:: /static/images/2013/02/firefox-proxy-configuration.png
    :align: center
    :alt: Firefox Proxy Configuration

.. code-block:: text

    Edit -> Preferences -> Advanced -> Network -> Settings -> Manual proxy configuration:
    HTTP Proxy:	127.0.0.1 Port:	3128
    Use this proxy server for all protocols 

Bash
~~~~

.. code-block:: bash
    
    $ vi ~/.bash_profile
    export http_proxy=http://127.0.0.1:3128/
    export ftp_proxy=ftp://127.0.0.1:3128/
    export socks_proxy=socks://127.0.0.1:3128/
    export https_proxy=https://127.0.0.1:3128/

.. [1] `CNTLM home page <http://cntlm.sourceforge.net>`_
