Installing Snoopy Logger
########################
:author: Leandro Toledo
:date: 2013-02-20 11:30
:category: GNU/Linux
:tags: sysadmin, howtos
:slug: installing-snoopy-logger
:summary: Using Snoopy Logger to log commands that were executed on the system.

Introduction
============

Snoopy Logger [1]_ is designed to aid system administrator by providing a log of
commands that were executed on the system. Logging is done via syslog.

Installation
============

It's pretty easy to install, just follow these instructions.

.. code-block:: bash

    $ git clone https://github.com/a2o/snoopy.git
    $ cd snoopy
    $ autoheader
    $ autoconf
    $ ./configure
    $ make
    $ su -c 'make install'

Configuration
=============

Now, enable it.

.. code-block:: bash

    $ su -c 'make enable'

Usage
=====

The exact location of Snoopy output depends on syslog configuration.

Usually it gets stored in one of the following files:

    - /var/log/auth
    - /var/log/messages
    - /var/log/secure

See an example output below:

.. code-block:: bash

    $ tail /var/log/auth.log
    Feb 20 12:08:47 oesp5209 snoopy[2643]: [uid:1000 sid:2572 tty:/dev/pts/3 cwd:/home/leandro filename:/bin/ls]: ls
    Feb 20 12:08:52 oesp5209 snoopy[2644]: [uid:1000 sid:2572 tty:/dev/pts/3 cwd:/home/leandro filename:/bin/pwd]: pwd

.. [1] `Snoopy Logger home page <https://github.com/a2o/snoopy>`_
