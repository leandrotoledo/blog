Debian Squeeze to Wheezy
########################
:author: Leandro Toledo
:date: 2013-04-09 12:00
:category: GNU/Linux
:tags: Debian, SysAdmin
:slug: debian-squeeze-to-wheezy
:summary: How to upgrade Debian Squeeze to Wheezy (new stable)

Introduction
============

Debian [1]_ is my favorite GNU/Linux distro. It's really simple to keep your
system updated. If you have a simple installation, in other words, a basic setup
without self-compiled programs or a complicated scenario, this article is for
you.

Configuration
=============

First of all you need to change your repository to the new one. So, change
squeeze to wheezy in your source.list.

.. code-block:: bash

   $ su -c 'vi /etc/apt/source.list'

.. code-block:: text

   deb http://ftp.us.debian.org/debian/ wheezy main contrib non-free
   deb-src http://ftp.us.debian.org/debian/ wheezy main contrib non-free
   deb http://security.debian.org/ wheezy/updates main contrib non-free
   deb-src http://security.debian.org/ wheezy/updates main contrib non-free

Installation
============

After that, update the index of packages from repository.

.. code-block:: bash

    $ su -c 'aptitude update'

Finally, upgrade the packages safety.

.. code-block:: bash

    $ su -c 'aptitude safe-upgrade'

You can check your release version using lsb_release.

.. code-block:: bash

    $ lsb_release -a
    No LSB modules are available.
    Distributor ID:	Debian
    Description:	Debian GNU/Linux 7.0 (wheezy)
    Release:	7.0
    Codename:	wheezy

.. [1] `Debian home page <http://www.debian.org/>`_
