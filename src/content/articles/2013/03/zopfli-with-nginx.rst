Zopfli with Nginx
#################
:author: Leandro Toledo
:date: 2013-03-01 13:30
:category: Nginx
:tags: sysadmin, howtos, nginx
:slug: zopfli-with-nginx
:summary: Using Google compression library, Zopfli, with Nginx

Introduction
============

Zopfli [1]_ is a new zlib (gzip, deflate) compatible compressor. This compressor
takes more time (~100x slower), but compresses around 5% better than zlib.
Importantly, though, the decompression speed is the same as deflated (e.g,
gzip). It's only slower during compression.  

It also is fully compatible with Nginx gzip module, this module will look for a
precompressed file in the same location that ends in ".gz". The purpose is to
avoid compressing the same file each time it is requested. 

Installation
============

It's pretty easy to install, just follow these instructions.

.. code-block:: bash

    $ git clone https://code.google.com/p/zopfli/
    $ cd zopfli
    $ make
    $ su -c 'cp zopfli /usr/local/bin/'

Configuration
=============

Now, enable Nginx gzip module.

.. code-block:: bash

    $ su -c 'vi /etc/nginx/sites-enabled/default' 

.. code-block:: text

    server {
        ...
        gzip_static on;
        ...
    }

.. code-block:: bash

    $ su -c 'nginx -s reload'

Usage
=====

Now, create the compressed file that will be automatically served by Nginx.

.. code-block:: bash

    $ zopfli /path/to/file

Or, make recursively.

.. code-block:: bash

    $ find . -type f -not -iname *.gz -exec zopfli {} \;

Zopfli also allows other levels of compression:

.. code-block:: bash

    $ zopfli -h
    Usage: zopfli [OPTION]... FILE
      -h    gives this help
      -c    write the result on standard output, instead of disk filename + '.gz'
      -v    verbose mode
      --gzip  output to gzip format (default)
      --deflate  output to deflate format instead of gzip
      --zlib  output to zlib format instead of gzip
      --i5  less compression, but faster
      --i10  less compression, but faster
      --i15  default compression, 15 iterations
      --i25  more compression, but slower
      --i50  more compression, but slower
      --i100  more compression, but slower
      --i250  more compression, but slower
      --i500  more compression, but slower
      --i1000  more compression, but slower

.. [1] `Zopfli Compression Algorithm home page <https://code.google.com/p/zopfli>`_
