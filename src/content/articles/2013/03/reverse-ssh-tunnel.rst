Reverse SSH Tunnel
##################
:author: Leandro Toledo
:date: 2013-03-20 12:00
:category: GNU/Linux
:tags: sysadmin, howtos
:slug: reverse-ssh-tunnel
:summary: How to create a reverse SSH tunnel

Introduction
============

OpenSSH [1]_ is a free version of the SSH connectivity tools that technical
users of the Internet rely on.  A reverse tunnel is a great feature to gain
access when the destination is behind a NAT or firewall.

Installation
============

You only need to follow some simple instructions.

.. code-block:: bash

    $ su -c 'aptitude install openssh-server'
    $ su -c 'aptitude install openssh-client'

Usage
=====

First of all you'll need a public IP with OpenSSH server. The server behind NAT
or firewall will make a connection that will open a SSH port, enabling a way to
connect trough this connection.

From the server that we want to open a way to access, open a reverse tunnel:

.. code-block:: bash

    $ ssh -N -f -R 2222:localhost:22 <user>@<host>

Now, you can connect to destination through SSH tunnel:

.. code-block:: bash

    $ ssh <user>@<host>
    $ ssh -p 2222 <user>@localhost

.. [1] `OpenSSH home page <http://www.openssh.org/>`_
