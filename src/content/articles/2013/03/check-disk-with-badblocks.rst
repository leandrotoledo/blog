Check disk with badblocks
#########################
:author: Leandro Toledo
:date: 2013-03-06 12:00
:category: GNU/Linux
:tags: sysadmin, howtos
:slug: check-disk-with-badblocks
:summary: How to check to test storage devices for bad blocks

Introduction
============

A bad sector is a sector on a computer's disk drive or flash memory that cannot
be used due to permanent damage, such as physical damage to the disk surface or
failed flash memory transistors.

badblocks is part of the e2fsprogs [1]_ package to test storage devices for bad
sectors. It creates a list of these sectors that can be used with other
programs.

Installation
============

Most of GNU/Linux distributions will have it on their repositories.

.. code-block:: bash

    $ su -c 'apt-get install e2fsprogs'

Usage
=====

badblocks has two main modes to detect bad sectors. They outputs a list of the
numbers of all bad sectors they can find. This list can be fed to mkfs or fsck
to be recorded in the filesystem data structures. Backup your data is always a
good idea, no matter if we'll check with destructive or non-destructive tests.

read-write test (destructive)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This test is primarily for testing new drives and is a read-write test. As the
pattern is written to every accesible block the device effectively gets wiped.

.. code-block:: bash

    $ su -c 'badblocks -wso bad-sectors /dev/<device>'

Options:

.. code-block:: text

    -w Use write-mode test. With this option, badblocks scans for bad blocks by writing some patterns (0xaa, 0x55, 0xff, 0x00) on every block of the device, reading every block and comparing the contents.

    -s Show the progress of the scan by writing out rough percentage completion of the current badblocks pass over the disk.

    -o Write the list of bad blocks to the specified file.

Now, you can pass this file to the mkfs command to record these bad sectors.

.. code-block:: bash

    $ su -c 'mkfs.ext4 -l bad-sectors /dev/<device>'

read-write test (non-destructive)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This test is designed for devices with data already on them. A non-destructive
read-write test does transparently backup original content before testing
sectors with a single random pattern and then restoring the content from the
backup.

.. code-block:: bash

    $ su -c 'badblocks -nso bad-sectors /dev/<device>'

Options:

.. code-block:: text

    -n Use non-destructive read-write mode.  By default only a non-destructive read-only test is done.

    -s Show the progress  of the scan by writing out rough percentage completion of the current badblocks pass over the disk.

    -o Write the list of bad blocks to the specified file.

Now, you can pass this file to the e2fsck command to record these bad sectors.

.. code-block:: bash

    $ su -c 'e2fsck -l bad-sectors /dev/<device>'

.. [1] `e2fsprogs home page <http://e2fsprogs.sourceforge.net/>`_
