How to restore GRUB 2 onto MBR
##############################
:author: Leandro Toledo
:date: 2013-03-07 16:00
:category: GNU/Linux
:tags: sysadmin, howtos
:slug: how-to-restore-grub-2-onto-mbr
:summary: How to restore GRUB 2 onto MBR

Introduction
============

GRUB [1]_ is a boot loader, briefly, a boot loader is the first software
program that runs when a computer starts. If the MBR (Master Boot Record) has
been overwritten or damaged, you can reinstall or recreate it, without having
to reinstall everything else.

Installation
============

Boot your system using a Live CD [2]_, I suggest Debian Live [3]_.

Most of GNU/Linux distributions will have GRUB natively.

.. code-block:: bash

    $ su -c 'apt-get install grub2-common'

Usage
=====

First of all you need to know which partition holds the GNU/Linux system you
want to boot and mount it.

.. code-block:: bash

    $ su -c 'mount /dev/sdaX /mnt'
    $ su -c 'mount --bind /proc /mnt/proc'
    $ su -c 'mount --bind /dev /mnt/dev'
    $ su -c 'mount --bind /sys /mnt/sys'

If you have a separate /boot partition, mount it too.

.. code-block:: bash

    $ su -c 'mount /dev/sdbY /mnt/boot'

Now chroot into your installed system and regenerate GRUB configuration file.

.. code-block:: bash

    $ su -c 'chroot /mnt update-grub2'

Then, umount and reboot.

.. code-block:: bash

    $ su -c 'umount /mnt/sys'
    $ su -c 'umount /mnt/dev'
    $ su -c 'umount /mnt/proc'
    $ su -c 'reboot'

.. [1] `GRUB home page <http://www.gnu.org/software/grub/>`_

.. [2] `The LiveCD List <http://livecdlist.com/operating-system/linux>`_

.. [3] `Debian Live home page <http://live.debian.net/>`_
